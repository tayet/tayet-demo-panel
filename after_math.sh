#!/bin/sh



# find the Dot Environment file and rename it to .env
# this is a workaround for the fact that the .env file
# is not copied to the Homestead machine
#

cd /var/www/alpha

if [ -f .env.production ]; then
    cp .env.production .env
    rm .env.production
fi

# reload the new .env file
source .env

